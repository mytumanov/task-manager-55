package ru.mtumanov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.mtumanov.tm.api.repository.dto.IDtoProjectRepository;
import ru.mtumanov.tm.api.repository.dto.IDtoTaskRepository;
import ru.mtumanov.tm.api.service.dto.IDtoProjectTaskService;
import ru.mtumanov.tm.dto.model.TaskDTO;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.entity.ProjectNotFoundException;
import ru.mtumanov.tm.exception.field.IdEmptyException;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public class ProjectTaskDtoService implements IDtoProjectTaskService {

    @NotNull
    @Autowired
    private ApplicationContext context;

    @NotNull
    protected EntityManager getEntityManager() {
        return context.getBean(EntityManager.class);
    }

    @NotNull
    protected IDtoTaskRepository getTaskRepository() {
        return context.getBean(IDtoTaskRepository.class);
    }

    @NotNull
    protected IDtoProjectRepository getProjectRepository() {
        return context.getBean(IDtoProjectRepository.class);
    }

    @Override
    public void bindTaskToProject(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId) throws AbstractException {
        @NotNull final IDtoTaskRepository repository = getTaskRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        if (projectId.isEmpty())
            throw new IdEmptyException();
        if (taskId.isEmpty())
            throw new IdEmptyException();
        if (!getProjectRepository().existById(userId, projectId))
            throw new ProjectNotFoundException();

        @NotNull final TaskDTO task = repository.findOneById(userId, taskId);
        try {
            task.setProjectId(projectId);
            entityManager.getTransaction().begin();
            repository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeProjectById(@NotNull final String userId, @NotNull final String projectId) throws AbstractException {
        @NotNull final IDtoTaskRepository repository = getTaskRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        if (projectId.isEmpty())
            throw new IdEmptyException();
        if (!getProjectRepository().existById(userId, projectId))
            throw new ProjectNotFoundException();

        @NotNull final List<TaskDTO> tasks = repository.findAllByProjectId(userId, projectId);
        try {
            entityManager.getTransaction().begin();
            for (TaskDTO task : tasks)
                repository.removeById(task.getId());
            getProjectRepository().removeById(userId, projectId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }

    }

    @Override
    public void unbindTaskFromProject(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId) throws AbstractException {
        @NotNull final IDtoTaskRepository repository = getTaskRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        if (projectId.isEmpty())
            throw new IdEmptyException();
        if (taskId.isEmpty())
            throw new IdEmptyException();
        if (!getProjectRepository().existById(userId, projectId))
            throw new ProjectNotFoundException();

        @NotNull final TaskDTO task = repository.findOneById(userId, taskId);
        task.setProjectId(null);
        try {
            entityManager.getTransaction().begin();
            repository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
