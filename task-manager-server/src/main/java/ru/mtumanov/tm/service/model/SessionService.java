package ru.mtumanov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import ru.mtumanov.tm.api.repository.model.ISessionRepository;
import ru.mtumanov.tm.api.service.model.ISessionService;
import ru.mtumanov.tm.model.Session;

public class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    @NotNull
    @Autowired
    private ApplicationContext context;

    @Override
    @NotNull
    protected ISessionRepository getRepository() {
        return context.getBean(ISessionRepository.class);
    }

}
