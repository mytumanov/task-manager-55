package ru.mtumanov.tm.endpoint;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.mtumanov.tm.api.service.IServiceLocator;
import ru.mtumanov.tm.dto.model.SessionDTO;
import ru.mtumanov.tm.dto.request.AbstractUserRq;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.user.AccessDeniedException;

public abstract class AbstractEndpoint {

    @NotNull
    @Getter
    @Autowired
    private IServiceLocator serviceLocator;

    @NotNull
    protected SessionDTO check(@Nullable final AbstractUserRq request, @Nullable final Role role) throws AbstractException {
        if (request == null || role == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        @NotNull final SessionDTO session = serviceLocator.getAuthService().validateToken(token);
        if (session.getRole() == null) throw new AccessDeniedException();
        if (!session.getRole().equals(role)) throw new AccessDeniedException();
        return session;
    }

    @NotNull
    protected SessionDTO check(@Nullable final AbstractUserRq request) throws AbstractException {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        return serviceLocator.getAuthService().validateToken(token);
    }

}
