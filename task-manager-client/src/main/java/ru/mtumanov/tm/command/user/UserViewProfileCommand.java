package ru.mtumanov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.dto.request.user.UserProfileRq;
import ru.mtumanov.tm.dto.response.user.UserProfileRs;
import ru.mtumanov.tm.exception.AbstractException;

@Component
public class UserViewProfileCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "view current user profile";
    }

    @Override
    @NotNull
    public String getName() {
        return "view-user-profile";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[USER VIEW PROFILE]");
        @NotNull final UserProfileRq request = new UserProfileRq(getToken());
        @NotNull final UserProfileRs response = getUserEndpoint().userProfile(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
            return;
        }
        showUser(response.getUser());
    }

}
