package ru.mtumanov.tm.command.system;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.api.service.ICommandService;
import ru.mtumanov.tm.api.service.IPropertyService;
import ru.mtumanov.tm.command.AbstractCommand;
import ru.mtumanov.tm.enumerated.Role;

@Component
public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return getServiceLocator().getCommandService();
    }

    protected IPropertyService getPropertyService() {
        return getServiceLocator().getPropertyService();
    }

    @Override
    @Nullable
    public Role[] getRoles() {
        return null;
    }
}
