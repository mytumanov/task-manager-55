package ru.mtumanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.dto.request.task.TaskShowByProjectIdRq;
import ru.mtumanov.tm.dto.response.task.TaskShowByProjectIdRs;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

@Component
public class TaskShowByProjectIdCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Show tasks by project id";
    }

    @Override
    @NotNull
    public String getName() {
        return "task-show-by-project-id";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @NotNull final TaskShowByProjectIdRq request = new TaskShowByProjectIdRq(getToken(), projectId);
        @NotNull final TaskShowByProjectIdRs response = getTaskEndpoint().taskShowByProjectId(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
            return;
        }
        renderTask(response.getTasks());
    }

}
