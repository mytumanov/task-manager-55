package ru.mtumanov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

@Component
public class ApplicationVersionCommand extends AbstractSystemCommand {

    @Override
    @NotNull
    public String getArgument() {
        return "-v";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show program version";
    }

    @Override
    @NotNull
    public String getName() {
        return "version";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println(getPropertyService().getApplicationVersion());
    }

}
