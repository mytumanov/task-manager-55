package ru.mtumanov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.api.endpoint.IDomainEndpoint;
import ru.mtumanov.tm.command.AbstractCommand;

@Component
public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    protected IDomainEndpoint getDomainEndpoint() {
        return serviceLocator.getDomainEndpoint();
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

}
