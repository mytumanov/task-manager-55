package ru.mtumanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.dto.request.project.ProjectStartByIdRq;
import ru.mtumanov.tm.dto.response.project.ProjectStartByIdRs;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

@Component
public class ProjectStartByIdCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Start project by id";
    }

    @Override
    @NotNull
    public String getName() {
        return "project-start-by-id";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[START PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectStartByIdRq request = new ProjectStartByIdRq(getToken(), id);
        @NotNull final ProjectStartByIdRs response = getProjectEndpoint().projectStartById(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
