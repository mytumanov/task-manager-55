package ru.mtumanov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.dto.request.data.DataYamlLoadFasterXmlRq;
import ru.mtumanov.tm.dto.response.data.DataYamlLoadFasterXmlRs;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;

@Component
public class DataYamlLoadFasterXmlCommand extends AbstractDataCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Load data from yaml file";
    }

    @Override
    @NotNull
    public String getName() {
        return "data-load-yaml";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[DATA LOAD YAML]");
        @NotNull final DataYamlLoadFasterXmlRs response = getDomainEndpoint().loadDataYamlFasterXml(new DataYamlLoadFasterXmlRq(getToken()));
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
