package ru.mtumanov.tm.component;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.api.endpoint.*;
import ru.mtumanov.tm.api.repository.ICommandRepository;
import ru.mtumanov.tm.api.service.*;
import ru.mtumanov.tm.command.AbstractCommand;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.system.ArgumentNotSupportedException;
import ru.mtumanov.tm.exception.system.CommandNotSupportedException;
import ru.mtumanov.tm.repository.CommandRepository;
import ru.mtumanov.tm.util.SystemUtil;
import ru.mtumanov.tm.util.TerminalUtil;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PROJECT_COMMANDS = "ru.mtumanov.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    @Getter
    @Autowired
    private ICommandService commandService;

    @NotNull
    @Getter
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    @Getter
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Getter
    @Autowired
    private ITokenService tokenService;

    @NotNull
    @Autowired
    private FileScanner fileScanner;

    @NotNull
    @Getter
    @Autowired
    private IProjectEndpoint projectEndpoint;

    @NotNull
    @Getter
    @Autowired
    private ISystemEndpoint systemEndpoint;

    @NotNull
    @Getter
    @Autowired
    private IDomainEndpoint domainEndpoint;

    @NotNull
    @Getter
    @Autowired
    private ITaskEndpoint taskEndpoint;

    @NotNull
    @Getter
    @Autowired
    private IUserEndpoint userEndpoint;

    @NotNull
    @Getter
    @Autowired
    private IAuthEndpoint authEndpoint;

    @NotNull
    @Getter
    @Autowired
    private AbstractCommand[] abstractCommands;

    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        try {
            Files.write(Paths.get(fileName), pid.getBytes());
        } catch (@NotNull final IOException e) {
            loggerService.error(e);
        }
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    public void start(@Nullable final String[] args) {
        init();

        processArguments(args);
        while (Thread.currentThread().isAlive()) {
            System.out.println("ENTER COMMAND:");
            try {
                @NotNull final String cmd = TerminalUtil.nextLine();
                processCommand(cmd);
                System.out.println("OK");
                loggerService.command(cmd);
            } catch (final Exception e) {
                System.out.println("FAIL");
                loggerService.error(e);
            }
        }
    }

    private void processArguments(@Nullable final String[] args) {
        if (args == null || args.length == 0 || args[0] == null)
            return;
        try {
            processArgument(args[0]);
            System.exit(0);
        } catch (final AbstractException e) {
            System.out.println(e.getMessage());
        }
    }

    private void processArgument(@NotNull String arg) throws AbstractException {
        if (arg.isEmpty())
            return;
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(arg);
        if (abstractCommand == null)
            throw new ArgumentNotSupportedException(arg);
        abstractCommand.execute();
    }

    public void processCommand(@NotNull final String command) throws AbstractException {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

    private void registry(@NotNull final AbstractCommand[] commands) {
        System.out.println(commands.length);
        for (@NotNull final AbstractCommand command : commands) {
            commandService.add(command);
        }
    }

    private void init() {
        loggerService.info("** WELCOME TO TASK_MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::stop));
        initPID();
        registry(abstractCommands);
        fileScanner.init();
    }

    private void stop() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
        fileScanner.stop();
    }

}
