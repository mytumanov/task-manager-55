package ru.mtumanov.tm.comparator;

import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.api.model.IHaveStatus;

import java.util.Comparator;

public enum StatusComparator implements Comparator<IHaveStatus> {

    INSTANCE;

    @Override
    public int compare(@Nullable final IHaveStatus o1, @Nullable final IHaveStatus o2) {
        if (o1 == null || o2 == null)
            return 0;
        if (o1.getStatus() == null || o2.getStatus() == null)
            return 0;
        return o1.getStatus().compareTo(o2.getStatus());
    }

}
