package ru.mtumanov.tm.dto.response.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.model.ProjectDTO;

@NoArgsConstructor
public final class ProjectUpdateByIdRs extends AbstractProjectRs {

    public ProjectUpdateByIdRs(@Nullable final ProjectDTO project) {
        super(project);
    }

    public ProjectUpdateByIdRs(@Nullable final Throwable err) {
        super(err);
    }

}